from django.urls import include, path

urlpatterns = [
    path("product/", include("marketplace.products.urls")),
]
