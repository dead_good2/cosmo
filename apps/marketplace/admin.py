from django.contrib import admin
from mptt.admin import MPTTModelAdmin

from .categories.models import Category
from .models import (
    Brand,
    Product,
    ProductColor,
    ProductReview,
    ProductVariant,
    ProductVariantImage,
    ProductVolume,
)

admin.site.register(Category, MPTTModelAdmin)


@admin.register(ProductColor)
class ProductColorAdmin(admin.ModelAdmin):
    list_display = ["name", "hex_code"]


@admin.register(ProductVolume)
class ProductVolumeAdmin(admin.ModelAdmin):
    list_display = ["volume_type", "volume_value"]


@admin.register(Brand)
class BrandAdmin(admin.ModelAdmin):
    list_display = ["title", "created_at"]
    search_fields = ["title"]


class ProductVariantImageInline(admin.StackedInline):
    model = ProductVariantImage
    extra = 1


@admin.register(ProductVariant)
class ProductVariantAdmin(admin.ModelAdmin):
    inlines = [ProductVariantImageInline]
    list_display = ("product", "color", "size", "price", "addition_price")
    list_filter = ("product", "color", "size")


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ("title", "article", "gender", "created_at")
    search_fields = ("title", "article")
    list_filter = ("gender", "brands", "category")


@admin.register(ProductReview)
class ProductReviewAdmin(admin.ModelAdmin):
    list_display = ("user", "product", "rating", "created_at")
    search_fields = ("user__username", "product__title")
    list_filter = ("rating", "created_at")
