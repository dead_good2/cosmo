from django.urls import path

from marketplace.products.views import (
    ProductAPIView,
)

urlpatterns = [
    path("product", ProductAPIView.as_view()),
]
