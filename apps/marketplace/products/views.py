from dataclasses import asdict
from typing import Any

from rest_framework import permissions, views
from rest_framework.response import Response

from .serializers import ProductSerializer
from .service import ProductService


class ProductAPIView(views.APIView):
    def __init__(
        self,
        service: ProductService,
        **kwargs: Any,
    ) -> None:
        self.service = service
        super().__init__(**kwargs)

    def post(self, request, *args: tuple, **kwargs: dict) -> Response:
        try:
            serializer = ProductSerializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            product_data = ProductSerializer(**serializer.validated_data)
            product = self.service.create_product(product_data)
            serializer = ProductSerializer(data=asdict(product))
            serializer.is_valid(raise_exception=True)
            return Response(serializer.data)
        except Exception as e:
            return Response({"error": str(e)}, status=400)

    def get(self, request, *args: tuple, **kwargs: dict) -> Response:
        try:
            product = self.service.get_product(request.product.title)
            serializer = ProductSerializer(data=asdict(product))
            serializer.is_valid(raise_exception=True)
            return Response(serializer.data)
        except Exception as e:
            return Response({"error": str(e)}, status=400)

    def put(self, request, *args: tuple, **kwargs: dict) -> Response:
        try:
            serializer = ProductSerializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            product_data_kwargs = {
                field: serializer.validated_data.get(field) for field in serializer.fields
            }
            product_data = ProductSerializer(**product_data_kwargs)
            product = self.service.update_product(product_data, request.product.title)
            serializer = ProductSerializer(data=asdict(product))
            serializer.is_valid(raise_exception=True)
            return Response(serializer.data)
        except Exception as e:
            return Response({"error": str(e)}, status=400)

    def delete(self, request, *args: tuple, **kwargs: dict) -> Response:
        try:
            product = self.service.get_product(request.product.title)
            self.service.delete_product(product)
            return Response({"success"})
        except Exception as e:
            return Response({"error": str(e)}, status=400)

    def get_permissions(self):
        if self.request.method in ["POST", "PUT", "DELETE"]:
            return [permissions.IsAuthenticated()]
        return [permissions.AllowAny()]
