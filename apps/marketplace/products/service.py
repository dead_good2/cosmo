from django.db.models import QuerySet

from .models import Product
from .repository import ProductRespository
from .types import ProductDataObject


class ProductError(Exception):
    class Code:
        PRODUCT_NOT_FOUND = "PRODUCT_NOT_FOUND"
        PRODUCT_EXISTS_WITH_SAME_TITLE = "PRODUCT_EXISTS_WITH_SAME_TITLE"


class ProductService:
    def __init__(
        self,
        repository: ProductRespository,
    ) -> None:
        self.repository = repository

    def get_all_products(self) -> QuerySet[Product]:
        return self.repository.get_all_products()

    def get_product(self, product_title: str) -> ProductDataObject:
        product = self.repository.get_product_by_title(product_title)
        if not product:
            return ProductError(ProductError.Code.PRODUCT_NOT_FOUND)
        return product

    def create_product(self, data: dict) -> ProductDataObject:
        product = self.repository.get_product_by_title(data.title)
        if product:
            return ProductError(ProductError.Code.PRODUCT_EXISTS_WITH_SAME_TITLE)
        product = self.repository.create_product(data)
        return product

    def update_product(self, data: dict, title: str) -> ProductDataObject:
        product_to_update = self.repository.get_product_by_title(title)
        if not product_to_update:
            return ProductError(ProductError.Code.PRODUCT_NOT_FOUND)
        updated_product = self.repository.update_product(data, product_to_update)
        return updated_product

    def delete_product(self, product: Product) -> None:
        self.repository.delete_product(product)
