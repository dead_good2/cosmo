from django.db.models.query import QuerySet

from .models import Product
from .types import ProductDataObject


class ProductRespository:
    def create_product(self, data: dict) -> ProductDataObject:
        return Product.objects.create(data=data)

    def get_all_products(self) -> QuerySet[Product]:
        return Product.objects.all()

    def get_product_by_title(self, product_title: str) -> ProductDataObject | None:
        try:
            product = Product.objects.get(title=product_title)
            return self._build_product_dto(product)
        except Product.DoesNotExist:
            return None

    def update_product(self, data: dict, instance: Product) -> ProductDataObject:
        for key, value in data.items():
            setattr(instance, key, value)
        instance.save()
        return instance

    def delete_product(self, instance: Product) -> None:
        instance.delete()

    def _build_product_dto(self, product: Product) -> ProductDataObject:
        return ProductDataObject(
            article=product.article,
            title=product.title,
            description=product.description,
            spec_description=product.spec_description,
            gender=product.spec_description,
            category=product.category.all(),
            brands=product.brands.all(),
        )
