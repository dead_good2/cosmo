from dataclasses import dataclass

from apps.marketplace.categories.models import Category

from .models import Brand


@dataclass
class ProductDataObject:
    article: str
    title: str
    description: str
    spec_description: str
    gender: str
    category: list[Category]
    brands: list[Brand]
