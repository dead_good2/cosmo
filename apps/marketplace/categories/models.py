from django.db import models
from django.utils.translation import gettext_lazy as _
from mptt.fields import TreeForeignKey
from mptt.models import MPTTModel

from marketplace.utils.abstract_models import BaseModel


class Category(MPTTModel, BaseModel):
    title = models.CharField(
        max_length=255,
        verbose_name=_("Title"),
    )
    image = models.ImageField(
        upload_to="images/categories",
        verbose_name=_("Image"),
        blank=True,
        null=True,
    )
    parent = TreeForeignKey(
        "self",
        on_delete=models.CASCADE,
        related_name="children",
        verbose_name=_("Parent"),
        blank=True,
        null=True,
    )
    display_top = models.BooleanField(default=False, verbose_name=_("Display top"))

    class Meta:
        verbose_name = _("Category")
        verbose_name_plural = _("Categories")

    def __str__(self):
        return self.title
