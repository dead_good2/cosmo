from rest_framework import serializers


class UserCreateSerializer(serializers.Serializer):
    username = serializers.CharField()
    email = serializers.EmailField()
    password = serializers.CharField()


class UserSerializer(serializers.Serializer):
    id = serializers.UUIDField()
    username = serializers.CharField()
    email = serializers.CharField()
    user_avatar = serializers.URLField(allow_null=True, required=False)
    auth_provider = serializers.CharField()


class UpdateUserDataSerializer(serializers.Serializer):
    username = serializers.CharField(required=False)
    user_avatar = serializers.ImageField(required=False)


class EmailResetPasswordSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True)


class ResetPasswordSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True)
    password = serializers.CharField(required=True)
    token = serializers.CharField(required=True)
