import datetime
import hashlib
import uuid

from django.contrib.auth.models import AbstractUser
from django.db import models

AUTH_PROVIDERS = {
    "facebook": "facebook",
    "google": "google",
    "twitter": "twitter",
    "email": "email",
}


class User(AbstractUser):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user_avatar = models.FileField(
        default=None,
        null=True,
        upload_to="profile_photos/",
        verbose_name="User Avatar",
    )
    is_courier = models.BooleanField(
        default=False,
        verbose_name="Is Courier",
    )
    is_warehousmen = models.BooleanField(
        default=False,
        verbose_name="Is Warehousmen",
    )
    is_client = models.BooleanField(
        default=False,
        verbose_name="Is Client",
    )
    auth_provider = models.CharField(
        max_length=255, blank=False, null=False, default=AUTH_PROVIDERS.get("email")
    )


class UserToken(models.Model):
    RESET_PASSWORD = "RESET_PASSWORD"
    ACTIVATION_USER = "ACTIVATION_USER"
    TOKEN_TYPE = {
        RESET_PASSWORD: "reset_password",
        ACTIVATION_USER: "activation user",
    }
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name="user_tokens",
    )
    token = models.CharField(max_length=255, null=True)
    token_type = models.CharField(
        max_length=15,
        choices=TOKEN_TYPE,
        default=ACTIVATION_USER,
    )
    email_verified = models.BooleanField(default=False)
    timestamp = models.DateTimeField(auto_now_add=True)
    valid_through = models.DateTimeField(null=True)

    def save(self, *args, **kwargs):
        if not self.token:
            data = f"{self.user.email}-{self.timestamp}"
            token = hashlib.sha256(data.encode("utf-8")).hexdigest()
            self.token = token
            self.valid_through = self.timestamp + datetime.timedelta(days=1)
        super(ActivationToken, self).save(*args, **kwargs)

    def __str__(self):
        return f"{self.token} -> {self.user.email}"

    class Meta:
        verbose_name = "User Token"
        verbose_name_plural = "User Tokens"
