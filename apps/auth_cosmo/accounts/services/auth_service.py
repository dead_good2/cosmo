import logging

from google.auth.transport import requests
from google.oauth2 import id_token

from auth_cosmo.accounts.repositories import AuthenticateRepository
from auth_cosmo.accounts.types import (
    GoogleSocialAuthDataCreate,
    UserAuthenticationDataCreate,
    UserAuthenticationDataObject,
)
from auth_cosmo.utils.exceptions import ServiceException

logger = logging.getLogger(__name__)


class AuthServiceError(ServiceException):
    class Code:
        PASSWORD_INVALID = "PASSWORD_INVALID"  # noqa: S105
        GOOGLE_TOKEN_EXPIRED_OR_INVALID = "GOOGLE_TOKEN_EXPIRED_OR_INVALID"  # noqa: S105


class AuthenticationService:
    def __init__(
        self,
        config: dict,
        authenticate_repository: AuthenticateRepository,
    ) -> None:
        self._config = config
        self._authenticate_repository = authenticate_repository

    def authenticate_by_email(
        self, user_object: UserAuthenticationDataCreate
    ) -> UserAuthenticationDataObject:
        user = self._authenticate_repository.authenticate_by_email(
            user_object.email, user_object.password
        )
        if user is not None:
            return user
        raise AuthServiceError(AuthServiceError.Code.PASSWORD_INVALID)

    def authenticate_by_google_get_user_info(
        self, auth_token: str
    ) -> UserAuthenticationDataObject:
        try:
            idinfo = id_token.verify_oauth2_token(auth_token, requests.Request())
            if "accounts.google.com" in idinfo["iss"]:
                return idinfo
        except Exception as e:
            logger.error("Google token verification failed: %s", e)
            raise AuthServiceError(AuthServiceError.Code.GOOGLE_TOKEN_EXPIRED_OR_INVALID) from e

    def authenticate_user_social(
        self, user_data: GoogleSocialAuthDataCreate
    ) -> UserAuthenticationDataObject:
        user = self._authenticate_repository.authenticate_social_user(user_data)
        return user
