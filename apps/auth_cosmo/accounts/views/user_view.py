from dataclasses import asdict
from typing import Any

from dependency_injector.wiring import Provide, inject
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import permissions, status, views
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.response import Response

from auth_cosmo.accounts.serializers import (
    EmailResetPasswordSerializer,
    ResetPasswordSerializer,
    UpdateUserDataSerializer,
    UserCreateSerializer,
    UserSerializer,
)
from auth_cosmo.accounts.services import UserService
from auth_cosmo.accounts.types import (
    UserDataCreate,
    UserUpdateData,
)
from auth_cosmo.container import Container


class UserAPIView(views.APIView):
    @inject
    def __init__(
        self,
        user_service: UserService = Provide[Container.user_package.user_service],
        **kwargs: Any,
    ) -> None:
        self._user_service = user_service
        super().__init__(**kwargs)

    parser_classes = (MultiPartParser, FormParser)

    @swagger_auto_schema(
        request_body=UserCreateSerializer,
        responses={status.HTTP_200_OK: openapi.Response("Response description", UserSerializer)},
    )
    def post(self, request, *args: tuple, **kwargs: dict) -> Response:
        try:
            serializer = UserCreateSerializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            user_data = UserDataCreate(**serializer.validated_data)
            user = self._user_service.create_user(user_data)
            serializer = UserSerializer(data=asdict(user))
            serializer.is_valid(raise_exception=True)
            return Response(serializer.data)
        except Exception as e:
            return Response({"error": str(e)}, status=400)

    @swagger_auto_schema(
        responses={status.HTTP_200_OK: openapi.Response("Response description", UserSerializer)},
    )
    def get(self, request, *args: tuple, **kwargs: dict) -> Response:
        try:
            user = self._user_service.get_user_by_email(request.user.email)
            serializer = UserSerializer(data=asdict(user))
            serializer.is_valid(raise_exception=True)
            return Response(serializer.data)
        except Exception as e:
            return Response({"error": str(e)}, status=400)

    @swagger_auto_schema(
        request_body=UpdateUserDataSerializer,
        responses={status.HTTP_200_OK: openapi.Response("Response description", UserSerializer)},
    )
    def put(self, request, *args: tuple, **kwargs: dict) -> Response:
        try:
            serializer = UpdateUserDataSerializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            user_data_kwargs = {
                field: serializer.validated_data.get(field) for field in serializer.fields
            }
            if serializer.validated_data.get("username") is None:
                user_data_kwargs["username"] = request.user.username
            user_data = UserUpdateData(**user_data_kwargs)
            user = self._user_service.update_user_data(user_data, request.user.email)
            serializer = UserSerializer(data=asdict(user))
            serializer.is_valid(raise_exception=True)
            return Response(serializer.data)
        except Exception as e:
            return Response({"error": str(e)}, status=400)

    def get_permissions(self):
        if self.request.method in ["GET", "PUT"]:
            return [permissions.IsAuthenticated()]
        return [permissions.AllowAny()]


class EmailSendResetPasswordAPIView(views.APIView):
    @inject
    def __init__(
        self,
        user_service: UserService = Provide[Container.user_package.user_service],
        **kwargs: Any,
    ) -> None:
        self._user_service = user_service
        super().__init__(**kwargs)

    @swagger_auto_schema(
        request_body=EmailResetPasswordSerializer,
        responses={status.HTTP_200_OK: openapi.Response('{"message": "Email send successfully"}')},
    )
    def post(self, request, *args: tuple, **kwargs: dict) -> Response:
        try:
            serializer = EmailResetPasswordSerializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            self._user_service.email_send_reset_password(serializer.validated_data["email"])
            return Response({"message": "Email send successfully"})
        except Exception as e:
            return Response({"error": str(e)}, status=400)


class ChangePasswordAPIView(views.APIView):
    @inject
    def __init__(
        self,
        user_service: UserService = Provide[Container.user_package.user_service],
        **kwargs: Any,
    ) -> None:
        self._user_service = user_service
        super().__init__(**kwargs)

    @swagger_auto_schema(
        request_body=ResetPasswordSerializer,
        responses={
            status.HTTP_200_OK: openapi.Response('{"message": "Password changed successfully"}')
        },
    )
    def post(self, request, *args: tuple, **kwargs: dict) -> Response:
        try:
            serializer = ResetPasswordSerializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            self._user_service.change_password(
                serializer.validated_data["email"],
                serializer.validated_data["password"],
                serializer.validated_data["token"],
            )
            return Response({"message": "Password changed successfully"})
        except Exception as e:
            return Response({"error": str(e)}, status=400)
