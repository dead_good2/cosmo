from dataclasses import asdict
from typing import Any

from dependency_injector.wiring import Provide, inject
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status, views
from rest_framework.response import Response

from auth_cosmo.accounts.serializers import (
    AuthenticationSerializer,
    GoogleSocialAuthSerializer,
    UserAuthenticationSerializer,
)
from auth_cosmo.accounts.services import AuthenticationService
from auth_cosmo.accounts.types import (
    GoogleSocialAuthDataCreate,
    UserAuthenticationDataCreate,
)
from auth_cosmo.container import Container


class ClientAuthenticationAPIView(views.APIView):
    @inject
    def __init__(
        self,
        authenticate_service: AuthenticationService = Provide[
            Container.authenticate_package.authenticate_service
        ],
        **kwargs: Any,
    ) -> None:
        self._authenticate_service = authenticate_service
        super().__init__(**kwargs)

    @swagger_auto_schema(
        request_body=UserAuthenticationSerializer,
        responses={
            status.HTTP_200_OK: openapi.Response("Response description", AuthenticationSerializer)
        },
    )
    def post(self, request, *args: tuple, **kwargs: dict) -> Response:
        try:
            serializer = UserAuthenticationSerializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            user_data = UserAuthenticationDataCreate(**serializer.validated_data)
            user = self._authenticate_service.authenticate_by_email(user_data)
            serializer = AuthenticationSerializer(data=asdict(user))
            serializer.is_valid(raise_exception=True)
            return Response(serializer.data)
        except Exception as e:
            return Response({"error": str(e)}, status=400)


class GoogleAuthAPIView(views.APIView):
    @inject
    def __init__(
        self,
        authenticate_service: AuthenticationService = Provide[
            Container.authenticate_package.authenticate_service
        ],
        **kwargs: Any,
    ) -> None:
        self._authenticate_service = authenticate_service
        super().__init__(**kwargs)

    @swagger_auto_schema(
        request_body=GoogleSocialAuthSerializer,
        responses={
            status.HTTP_200_OK: openapi.Response("Response description", AuthenticationSerializer)
        },
    )
    def post(self, request, *args: tuple, **kwargs: dict) -> Response:
        try:
            serializer = GoogleSocialAuthSerializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            google_user = self._authenticate_service.authenticate_by_google_get_user_info(
                serializer.validated_data["auth_token"]
            )
            google_user_data = GoogleSocialAuthDataCreate(
                first_name=google_user["given_name"],
                last_name=google_user["family_name"],
                email=google_user["email"],
                picture_url=google_user["picture"],
            )
            user = self._authenticate_service.authenticate_user_social(google_user_data)
            serializer = AuthenticationSerializer(data=asdict(user))
            serializer.is_valid(raise_exception=True)
            return Response(serializer.data)
        except Exception as e:
            return Response({"error": str(e)}, status=400)
