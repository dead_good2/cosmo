from django.urls import path

from auth_cosmo.accounts.views import (
    ChangePasswordAPIView,
    EmailSendResetPasswordAPIView,
    UserAPIView,
)

urlpatterns = [
    path("user/", UserAPIView.as_view()),
    path("reset-password/", EmailSendResetPasswordAPIView.as_view()),
    path("change-password/", ChangePasswordAPIView.as_view()),
]
