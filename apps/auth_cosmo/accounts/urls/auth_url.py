from django.urls import path

from auth_cosmo.accounts.views import (
    ClientAuthenticationAPIView,
    GoogleAuthAPIView,
)

urlpatterns = [
    path("client/", ClientAuthenticationAPIView.as_view()),
    path("google/", GoogleAuthAPIView.as_view()),
]
